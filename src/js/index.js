//require the express nodejs module
var express = require('express');
//set an instance of express
var app = express();
//require the body-parser nodejs module
var bodyParser = require('body-parser');
//require the path nodejs module
//var path = require("path");

//support parsing of application/json type post data
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

var models = require('express-cassandra');	
//Tell express-cassandra to use the models-directory, and
//use bind() to load the models using cassandra configurations.
models.setDirectory( __dirname + '/models').bind(
    {
        clientOptions: {
            contactPoints: ['127.0.0.1'],
            protocolOptions: { port: 9042 },
            keyspace: 'samplekeyspace',
            queryOptions: {consistency: models.consistencies.one}
        },
        ormOptions: {
            defaultReplicationStrategy : {
                class: 'SimpleStrategy',
                replication_factor: 1
            },
            migration: 'safe'
        }
    },
    function(err) {
        if(err) throw err;

        // You'll now have a `person` table in cassandra created against the model
        // schema you've defined earlier and you can now access the model instance
        // in `models.instance.Person` object containing supported orm operations.
    }
);

//------------------------------------------
var mqtt = require('mqtt');
var MQTT_TOPIC          = "hello";
var MQTT_ADDR           = "tcp://127.0.0.1";
var MQTT_PORT           = 1883;

var client  = mqtt.connect(MQTT_ADDR,{
	username:"hassan",
	password:"12345",
	clean: true,
	clientId: 'test-client-id', 
	protocolId: 'MQIsdp', 
	protocolVersion: 3, 
	connectTimeout:1000, 
	debug:true
});

client.on('connect', (connack) => {  
  if (connack.sessionPresent) {
    console.log('Already subbed, no subbing necessary');
  } else {
    console.log('First session! Subbing.');
    client.subscribe(MQTT_TOPIC, { qos: 1 });
  }
});

client.on('message', (topic, message) => {
	//console.log(message.toString());
    console.log(`Received message: ${message}`);
    if(message.toString().startsWith("{")){
    	var json = JSON.parse(message.toString());
    	/*console.log(json.userid);
    	if(json){    	
    		console.log(json.userid);
		}*/
		var user_loc = new models.instance.UserLocation({
    				user_id 	: json.userid,
        			latitude    : json.lat,
        			longitude   : json.lng,
        			created_at  : Date.now(),
        			action      : json.action,
        			job_id      : json.jobId,
        			battery     : json.battery
		});
	
		user_loc.save(function(err){
    		if(err) 
        		console.log(err);  
    		else
    			console.log('Saved!');
		});
	}
	/*message = JSON.stringify(message);
	if(message.startsWith("{")){
		if(JSON.parse(message)){
			var json = JSON.parse(message);
			console.log(json);
		}

	}*/
	//console.log(message.userid);
	
});


//------------------------------------------


app.get('/greet', function (req, res) {
	console.log('GET');
  	res.send('Hello World!');
});

/*app.post('/samplePost', function(req, res){
	console.log(req.body);
	res.send(JSON.stringify({
            firstName: req.body.firstName || null,
            lastName: req.body.lastName || null
        }));
});


app.post('/location', function(req, res){
	console.log(req.body);
	var user_loc = new models.instance.UserLocation({
    				user_id 	: req.body.userid,
        			latitude    : req.body.lat,
        			longitude   : req.body.lng,
        			created_at  : Date.now()
	});
	
	user_loc.save(function(err){
    	if(err) {
        	console.log(err);        	
        	res.sendStatus(500);
    	}else
    		console.log('Saved!');
	});
   res.sendStatus(200);
});*/


app.listen(5000, function () {
  console.log('Example app listening on port 5000!');
});
