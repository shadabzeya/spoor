module.exports = {
    fields:{
        user_id 	: "int",
        latitude    : "float",
        longitude   : "float",
        created_at  : "timestamp",
        action      : "text",
        job_id      : "int",
        battery     : "int"
    },
    key:["user_id", "created_at"]
}

/*class UserLocationModel {
	constructor(user_id, latitude, longitude, created_at){
		this.user_id = user_id;
		this.latitude = latitude;
		this.longitude = longitude;
		this.created_at = created_at;
		console.log(`'Created UserLocationModel via class: ${user_id}'`);
	}
}
exports.UserLocationModel; */